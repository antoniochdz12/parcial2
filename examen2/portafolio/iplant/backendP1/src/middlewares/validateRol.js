export const isUser = (req, res, next) => {
    if (req.user.role === 'User') {
        return next();
    }
    return res.status(403).json({ message: 'Acceso denegado. Necesitas ser un usuario.' });
};

export const isAdmin = (req, res, next) => {
    if (req.user.role === 'Administrador') {
        return next();
    }
    return res.status(403).json({ message: 'Acceso denegado. Necesitas ser un administrador.' });
};

export const isInstaller = (req, res, next) => {
    if (req.user.role === 'Instalador') {
        return next();
    }
    return res.status(403).json({ message: 'Acceso denegado. Necesitas ser un superadministrador.' });
};

export const isAlmacenista = (req, res, next) => {
    if (req.user.role === 'Almacenista') {
        return next();
    }
    return res.status(403).json({ message: 'Acceso denegado. Necesitas ser un superadministrador.' });
};
