import { getInstalaciones, getInstalacion, createInstalacion, updateInstalacion, deleteInstalacion } from "../../controllers/instalacion/instalacion.controller.js";
import { Router } from "express";


const router = Router();

router.get("/instalaciones", getInstalaciones)
router.get("/instalacion/:id", getInstalacion)
router.post("/instalacion", createInstalacion)
router.patch("/instalacion/:id", updateInstalacion)
router.delete("/instalacion/:id", deleteInstalacion)

export default router;