import mongoose from "mongoose";

const suscripcionSchema = new mongoose.Schema({
    user: { type: String, required: true },
    activo: { type: Boolean, default: true},
    instalacion: {type: Boolean, default: null},
    location: { type: String, default: 'No requerida'},
    codigo: { type: String, required: true}
  });
  
export default mongoose.model('Suscription', suscripcionSchema);
  