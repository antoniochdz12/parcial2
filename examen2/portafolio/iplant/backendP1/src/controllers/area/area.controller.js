import Area from "../../models/area/area.model.js";
import {Dispositivo} from "../../models/dispositivo/dispositivo.model.js"
import Paquete from "../../models/paquete/paquete.model.js"

export const getAreas = async(req, res) => {
    try {
        const areas = await Area.find();
        res.status(200).json(areas);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const getArea = async(req, res) => {
    const { id } = req.params;

    try {
        const area = await Area.findOne({ _id: id });
        res.status(200).json(area);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const createArea = async(req, res) => {
    const { name, codigo} = req.body;

    try {
        if(!name || !codigo ){
            return res.status(400).json({ message: 'Se requiere el campo name, director y codigo' });
        }

        const existDirector = await Suscripcion.findById(req.user._id)
        if (!existDirector) {
            return res.status(404).json({ message: 'Director no encontrado' });
        }
        if (codigo) {
            const existCodigo = await Paquete.findOne({ id: codigo });
            if (!existCodigo) {
                return res.status(404).json({ message: 'Paquete no encontrado' });
            }
            const dispositivos = existCodigo.dispositivos;
        } else {
            // Si no hay codigo, asignar un arreglo vacío a dispositivos para evitar errores
            const dispositivos = [];
        }
        
        const area = new Area({ name, director: existDirector.id , codigo, dispositivos });
        await area.save();
        res.status(201).json(area);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const updateArea = async (req, res) => {
    const { id } = req.params;
    const { name, director, codigo, dispositivos } = req.body;

    try {
        const updatedArea = await Area.findOneAndUpdate(
            { id: id },
            { name, dispositivos },
            { new: true, runValidators: true }
        );

        if (!updatedArea) {
            return res.status(404).json({ message: "Area no encontrada" });
        }

        res.status(200).json(updatedArea);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const deleteArea = async(req, res) => {
    const { id } = req.params;
    try {
        const area = await Area.findOneAndDelete({ id: id });
        res.status(200).json(area);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}